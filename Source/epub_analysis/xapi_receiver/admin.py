from django.contrib import admin
from .models import Book, Object, Choice, Record

# Register your models here.
admin.site.register(Book)
admin.site.register(Object)
admin.site.register(Choice)
admin.site.register(Record)