# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('book_name', models.CharField(max_length=200)),
                ('home_page', models.CharField(max_length=200)),
                ('object_type', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('choice_id', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100)),
                ('count', models.IntegerField(default=0)),
                ('type', models.CharField(max_length=10)),
                ('language', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Object',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.CharField(default=b'http://adlnet.gov/expapi/activities/example', max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=20)),
                ('interaction_type', models.CharField(max_length=200)),
                ('object_type', models.CharField(max_length=20)),
                ('book', models.ForeignKey(to='xapi_receiver.Book')),
            ],
        ),
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('record_id', models.CharField(default=b'a8803f1f-9f0d-455b-8c6a-c2c6cf50e70a', max_length=50)),
                ('response', models.CharField(max_length=200)),
                ('stored', models.CharField(max_length=50)),
                ('timestamp', models.CharField(max_length=50)),
                ('version', models.CharField(max_length=10)),
                ('object', models.ForeignKey(to='xapi_receiver.Object')),
            ],
        ),
        migrations.AddField(
            model_name='choice',
            name='object',
            field=models.ForeignKey(to='xapi_receiver.Object'),
        ),
    ]
