# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xapi_receiver', '0002_record_success'),
    ]

    operations = [
        migrations.AddField(
            model_name='object',
            name='count',
            field=models.IntegerField(default=1),
        ),
    ]
