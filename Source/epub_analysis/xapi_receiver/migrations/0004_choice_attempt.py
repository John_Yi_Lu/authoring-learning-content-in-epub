# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xapi_receiver', '0003_object_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='attempt',
            field=models.IntegerField(default=0),
        ),
    ]
