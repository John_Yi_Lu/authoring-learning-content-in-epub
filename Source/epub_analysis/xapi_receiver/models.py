from django.db import models

# Create your models here.
class Book(models.Model):
    book_name = models.CharField(max_length=200)
    home_page = models.CharField(max_length=200)
    object_type = models.CharField(max_length=20)
    
    def __unicode__(self):
        return self.book_name

class Object(models.Model):
    object_id = models.CharField(max_length=100, default="http://adlnet.gov/expapi/activities/example")
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=20)
    interaction_type = models.CharField(max_length=200)
    object_type = models.CharField(max_length=20)
    count = models.IntegerField(default=1)
    book = models.ForeignKey(Book)
    
    def __unicode__(self):
        return self.name
    
class Record(models.Model):
    record_id = models.CharField(max_length=50, default="a8803f1f-9f0d-455b-8c6a-c2c6cf50e70a")
    success = models.BooleanField(default=False)
    response = models.CharField(max_length=200)
    stored = models.CharField(max_length=50)
    timestamp = models.CharField(max_length=50)
    version = models.CharField(max_length=10)
    object = models.ForeignKey(Object)
    
    def __unicode__(self):
        return self.record_id
    
class Choice(models.Model):
    choice_id = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    count = models.IntegerField(default=0)
    attempt = models.IntegerField(default=0)
    type = models.CharField(max_length=10)
    language = models.CharField(max_length=10)
    object = models.ForeignKey(Object)
    
    def __unicode__(self):
        return self.choice_id
    
