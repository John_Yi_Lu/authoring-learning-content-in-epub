from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^update_logs/$', views.update_logs, name='update_logs'),
]