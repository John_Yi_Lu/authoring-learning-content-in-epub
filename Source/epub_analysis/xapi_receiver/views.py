from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseServerError
from .models import *
import json
from ctypes.test.test_internals import ObjectsTestCase

# Create your views here.
def index(request):
    return render(request, 'index.html')

def update_logs(request):
    print '------------------------------------'
    data = json.loads(request.POST['logs'])
    logs = data['data']
    bookname = request.POST.get('bookname', "")
    process_logs(logs, bookname)
    stats = prepare_book_stats(bookname)
    return HttpResponse(json.dumps(stats))

def prepare_book_stats(bookname):
    print 'inside'
    booklist = []
    if bookname == "":
        books = Book.objects.all()
        for book in books:
            out_booklist = prepare_book_stats(book.book_name)
            booklist += out_booklist
            #print booklist
        
    else:
        book = Book.objects.filter(book_name__exact=bookname)
        out_book = {}
        print book
        out_objects = []
        objects = Object.objects.filter(book__exact=book)
        print objects
        
        for obj in objects:
            out_object = {}
            choices = Choice.objects.filter(object__exact=obj).values('choice_id', 'description',
                                                                                    'count', 'type')
            out_object['name'] = obj.name
            out_object['type'] = obj.interaction_type
            out_object['count'] = obj.count
            out_object['choices'] = list(choices)
            out_objects.append(out_object.copy())
        out_book['info'] = bookname.decode('unicode-escape')
        out_book['objects'] = out_objects
        #print out_book
        booklist.append(out_book.copy())
    return booklist

def process_logs(logs, bookname=""):
    for i, log in enumerate(logs):
        #print i
        #print_log(log)
        # If the record is already in DB, do not process it
        if Record.objects.filter(record_id=log['id']).exists():
            continue
        # Only process specified books
        if bookname != "":
            if bookname != log['actor']['account']['name']:
                continue
        # Create book
        book, created = Book.objects.get_or_create(book_name=log['actor']['account']['name'],
                                                   home_page=log['actor']['account']['homePage'],
                                                   object_type=log['actor']['objectType'])
        print book
        # Create object info
        interaction_type = log['object']['definition']['interactionType']
        if interaction_type == "other":
            interaction_type = "video" if log['result']['response'].startswith("video") else "audio"
            
        book_object, created = Object.objects.get_or_create(object_id=log['object']['id'],
                        name=log['object']['definition']['name']['en-US'],
                        description=log['object']['definition']['description']['en-US'],
                        type=log['object']['definition']['type'],
                        interaction_type=interaction_type,
                        object_type=log['object']['objectType'],
                        book=book)
        
        print book_object
        
        print log['result']['response']
        
        # Update choice count
        sources = parse_str_to_list(log['result']['response'])
        #correct_sources, correct_targets = parse_str_to_list(log['object']['definition']['interactionType'][0])
        
        print sources
        print book_object.interaction_type
        
        # Create choices for the first time object created
        if created:
            if book_object.interaction_type == "choice":
                for raw_choice in log['object']['definition']['choices']:
                    count = 1 if raw_choice['id'] in sources else 0
                    choice = Choice(choice_id=raw_choice['id'],
                                    description=raw_choice['description']['en-US'],
                                    count=count,
                                    type="choice",
                                    language='en-US',
                                    object=book_object)
                    choice.save()
            elif book_object.interaction_type == "sequencing":
                for raw_choice in log['object']['definition']['choices']:
                    count = 1 if raw_choice['description']['en-US'] in sources else 0
                    choice = Choice(choice_id=raw_choice['id'],
                                    description=raw_choice['description']['en-US'],
                                    count=count,
                                    type="sequencing",
                                    language='en-US',
                                    object=book_object)
                    choice.save()
            elif book_object.interaction_type == "matching":
                correct_sources = parse_str_to_list(log['object']['definition']['correctResponsesPattern'][0])
                for i, correct_source in enumerate(correct_sources):
                    print correct_source
                    count = 1 if correct_source in sources else 0
                    choice = Choice(choice_id=str(i),
                                    description=correct_source,
                                    count=count,
                                    type="matching",
                                    language='en-US',
                                    object=book_object)
                    choice.save()
            elif book_object.interaction_type == "long-fill-in":
                fillin = Choice(choice_id=log['object']['definition']['name']['en-US'],
                                    description=log['object']['definition']['description']['en-US'],
                                    type="fillin",
                                    language='en-US',
                                    object=book_object)
                fillin.save()
            else:
                media = Choice(choice_id=log['object']['definition']['name']['en-US'],
                                    description=log['object']['definition']['description']['en-US'],
                                    type="media",
                                    language='en-US',
                                    object=book_object)
                media.save()
        # Only update count
        else:
            book_object.count += 1;
            if book_object.interaction_type == "choice":
                choices = Choice.objects.filter(choice_id__in=sources, type__exact="choice", object__exact=book_object)
                for choice in choices:
                    choice.count += 1
                    choice.save()
            elif book_object.interaction_type == "sequencing":
                choices = Choice.objects.filter(description__in=sources, type__exact="sequencing", object__exact=book_object)
                for choice in choices:
                    choice.count += 1
                    choice.save()
            elif book_object.interaction_type == "matching":
                choices = Choice.objects.filter(description__in=sources, type__exact="matching", object__exact=book_object)
                for choice in choices:
                    choice.count += 1
                    choice.save()
            elif book_object.interaction_type == "long-fill-in":
                choice = Choice.objects.filter(choice_id__exact=log['object']['definition']['name']['en-US'], 
                                               description__exact=log['object']['definition']['description']['en-US'],
                                               type__exact="fillin", object__exact=book_object)
            else:
                choice = Choice.objects.filter(choice_id__exact=log['object']['definition']['name']['en-US'], 
                                               description__exact=log['object']['definition']['description']['en-US'],
                                               type__exact="media", object__exact=book_object)
                
        print "finish choices"
        
        record = Record(record_id=log['id'],
                        success=log['result']['success'],
                        response=log['result']['response'],
                        stored=log['stored'],
                        timestamp=log['timestamp'],
                        version=log['version'],
                        object=book_object)
        record.save()
        print record
        print "-------------------end---------------------"
        
def parse_str_to_list(liststr):
    sources = liststr.split("[,]")
    print sources
    return sources

def print_log(log):
    print "id:            " + log['id']
    print "stored:        " + log['stored']
    
    print "actor"
    print "  name:        " + log['actor']['account']['name']
    print "  homepage:    " + log['actor']['account']['homePage']
    
    print "verb"
    print "  id:          " + log['verb']['id']
    print "  display:     " + log['verb']['display']['en-US']
    
    print "object"
    print "  name:        " + log['object']['definition']['name']['en-US']
    print "  description: " + log['object']['definition']['description']['en-US']
    print "  type:        " + log['object']['definition']['interactionType']
    print "----------------------------------------------------------"


