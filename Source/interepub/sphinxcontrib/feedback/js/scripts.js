/* Initialize */

// Initialize the editor

var editor = document.getElementById("editor");

$( document ).ready(function() {
    var editorDiv = $( "<div id='editor'></div>" );
    $("body").append(editorDiv);
    editor = ace.edit("editor");
    editor.getSession().setMode("ace/mode/javascript");
    editor.on('change', function() {
        var r = validateJSON(editor.getValue());
        //btnValidateJSON(r);
    });
});

// Interactive component config
var componentSettings = {
    id: eBookConfig.activityId,
    name: "",
    description: "",
    type: "http://adlnet.gov/expapi/activities/cmi.interaction",
    interactionType: "",
    correctResponsesPattern: [],
    result: {
        "success": false,
        "response": ""
    }
}

var accountAgent = {
  "name": eBookConfig.bookname,
  "homePage": eBookConfig.homepage
}

stmts = [];

// Override any credentials put in the XAPIWrapper.js
function setupConfig() {
    // get LRS credentials from user interface
    var endpoint = eBookConfig.endpoint;
    var user = eBookConfig.user;
    var password = eBookConfig.password;

    var conf = {
        "endpoint" : endpoint,
        "auth" : "Basic " + toBase64(user + ":" + password),
    };
    ADL.XAPIWrapper.changeConfig(conf);
}

// Build statement from the GUI
function buildStatement() {
    var stmt = {};
    
    stmt['actor'] = {};
    stmt['actor']['account'] = $.parseJSON(JSON.stringify(accountAgent, undefined, 4));
    stmt['actor']['objectType'] = "Agent";
    
    stmt['verb'] = {};
    stmt['verb']['id'] = "http://adlnet.gov/expapi/verbs/interacted";
    stmt['verb']['display'] = {};
    stmt['verb']['display'][eBookConfig.language] = "interacted";
    
    stmt['object'] = {};
    stmt['object']['id'] = componentSettings.id;
    stmt['object']['definition'] = {};
    stmt['object']['definition']['name'] = {};
    stmt['object']['definition']['name'][eBookConfig.language] = componentSettings.name;
    stmt['object']['definition']['description'] = {};
    stmt['object']['definition']['description'][eBookConfig.language] = componentSettings.description;
    stmt['object']['definition']['type'] = componentSettings.type;
    stmt['object']['definition']['interactionType'] = componentSettings.interactionType;
    stmt['object']['definition']['correctResponsesPattern'] = componentSettings.correctResponsesPattern;
    switch (componentSettings.interactionType) {
        case "choice":
        case "sequencing":
            stmt['object']['definition']['choices'] = componentSettings.choices;
            break;
        case "matching":
            stmt['object']['definition']['source'] = componentSettings.source;
            stmt['object']['definition']['target'] = componentSettings.target;
            break;
        default:
    }
    stmt['object']['objectType'] = "Activity";
    
    stmt['result'] = componentSettings.result;
    
    console.log(stmt);
    return stmt;
}

// Generate statement and preview in the editor
function previewStatement() {
    var stmt = buildStatement();

    editor.setValue(JSON.stringify(stmt, undefined, 4)); // or session.setValue
    editor.clearSelection(); // or session.setValue
}

// Send statement to the LRS
function sendStatement() {
    setupConfig();
    
    previewStatement();

    var stmt = editor.getValue(); // or session.getValue

    if (validateJSON(stmt) != true) { // JSON is invalid
        //notify({ message: "invalid JSON, cannot send" }, notificationErrorSettings);
        return false;
    }

    var xstmt = $.parseJSON(stmt);

    ADL.XAPIWrapper.sendStatement(xstmt, function(r, obj) {
        console.log(r);
        //console.log(obj);
        // notification
        if (r.status == 200) {
            //notify({ title: "Status " + r.status + " " + r.statusText + ": ", message: "<b><em>" + xstmt.verb.display['en-US'] + "</em></b> statement sent successfully to LRS" }, notificationSettings);
        }
        //var prettyStatement = styleStatementView(xstmt.id, xstmt);
        //$("#sent-statements").append(prettyStatement);
        //PR.prettyPrint();
    });
}

// Validate JSON
function validateJSON(json) {
    try {
        var c = $.parseJSON(json);
        return true;
    } catch (err) {
        return err;
    }
}

// Statement configuration when triggered
function videoPlay(ele) {
    componentSettings.name = ele.id;
    componentSettings.description = ele.innerHTML.trim();
    componentSettings.interactionType = "other";
    componentSettings.result["success"] = true;
    componentSettings.result["response"] = "video:" + ele.src;
    
    //console.log(componentSettings);
    sendStatement();
}

function audioPlay(ele) {
    componentSettings.name = ele.id;
    componentSettings.description = ele.innerHTML.trim();
    componentSettings.interactionType = "other";
    componentSettings.result["success"] = true;
    componentSettings.result["response"] = "audio:" + ele.src;
    
    //console.log(componentSettings);
    sendStatement();
}

function sendMCMFFeedback(ele) {
    //console.log(ele);
    componentSettings.name = ele.divid;
    componentSettings.description = ele.question.trim();
    componentSettings.interactionType = "choice";
    componentSettings.correctResponsesPattern = [];
    componentSettings.correctResponsesPattern.push(ele.correctList[0]);
    componentSettings.choices = [];
    for (var i = 0; i < ele.answerList.length; i++) {
        var choice = {}
        choice["id"] = ele.answerList[i].id;
        choice["description"] = {};
        choice["description"][eBookConfig.language] = ele.answerList[i].content;
        componentSettings.choices.push(choice);
    }
    
    componentSettings.result["success"] = ele.correct;
    componentSettings.result["response"] = ele.answerList[ele.givenArray[0]].id;
    
    //console.log(componentSettings);
    sendStatement();
}

function sendMCMAFeedback(ele) {
    //console.log(ele);
    componentSettings.name = ele.divid;
    componentSettings.description = ele.question.trim();
    componentSettings.interactionType = "choice";
    var correctResultString = "";
    for (var i = 0; i < ele.correctList.length; i++) {
        if (0 == i) {
            correctResultString = ele.correctList[i];
        } else {
            correctResultString += ("[,]" + ele.correctList[i]);
        }
    }
    componentSettings.correctResponsesPattern = [];
    componentSettings.correctResponsesPattern.push(correctResultString);
    componentSettings.choices = [];
    for (var i = 0; i < ele.answerList.length; i++) {
        var choice = {}
        choice["id"] = ele.answerList[i].id;
        choice["description"] = {};
        choice["description"][eBookConfig.language] = ele.answerList[i].content;
        componentSettings.choices.push(choice);
    }
    
    componentSettings.result["success"] = ele.correct;
    var responseString = "";
    for (var i = 0; i < ele.givenArray.length; i++) {
        if (0 == i) {
            responseString = ele.answerList[ele.givenArray[i]].id;
        } else {
            responseString += ("[,]" + ele.answerList[ele.givenArray[i]].id);
        }
    }
    componentSettings.result["response"] = responseString;
    
    //console.log(componentSettings);
    sendStatement();
}

RegExp.escape= function(s) {
    return s.replace(/[\\b]/gi, '');
}

function sendFITBFeedback(ele) {
    //console.log(ele);
    componentSettings.name = ele.divid;
    var questionString = "";
    for (var i = 0; i < ele.children.length; i++) {
        if (0 == i) {
            questionString = ele.children[i].firstChild.textContent.trim();
        } else {
            questionString += ("[,]" + ele.children[i].firstChild.textContent.trim());
        }
    }
    componentSettings.description = questionString;
    componentSettings.interactionType = "long-fill-in";
    var correctResultString = "{case_matters=" + !ele.casei + "}";
    for (var i = 0; i < ele.correctAnswerArray.length; i++) {
        if (0 == i) {
            correctResultString += RegExp.escape(ele.correctAnswerArray[i]);
        } else {
            correctResultString += ("[,]" + RegExp.escape(ele.correctAnswerArray[i]));
        }
    }
    componentSettings.correctResponsesPattern = [];
    componentSettings.correctResponsesPattern.push(correctResultString);
    
    componentSettings.result["success"] = ele.correct;
    var responseString = "";
    for (var i = 0; i < ele.given_arr.length; i++) {
        if (0 == i) {
            responseString = ele.given_arr[i];
        } else {
            responseString += ("[,]" + ele.given_arr[i]);
        }
    }
    componentSettings.result["response"] = responseString;
    
    //console.log(componentSettings);
    sendStatement();
}

function sendParsonsFeedback(ele) {
    //console.log(ele);
    componentSettings.name = ele.divid;
    componentSettings.description = ele.question.trim();
    componentSettings.interactionType = "sequencing";
    // Set correct result and choices at the same time
    var correctResultString = "";
    componentSettings.choices = [];
    for (var i = 0; i < ele.model_solution.length; i++) {
        if (0 == i) {
            correctResultString = ele.model_solution[i].code;
        } else {
            correctResultString += ("[,]" + ele.model_solution[i].code);
        }
        var choice = {}
        choice["id"] = i.toString();
        choice["description"] = {};
        choice["description"][eBookConfig.language] = ele.model_solution[i].code;
        componentSettings.choices.push(choice);
    }
    componentSettings.correctResponsesPattern = [];
    componentSettings.correctResponsesPattern.push(correctResultString);
    
    componentSettings.result["success"] = ele.grader.success;
    var responseString = "";
    for (var i = 0; i < ele.grader.lines.length; i++) {
        if (0 == i) {
            responseString = ele.model_solution[ele.grader.lines[i]].code;
        } else {
            responseString += ("[,]" + ele.model_solution[ele.grader.lines[i]].code);
        }
    }
    componentSettings.result["response"] = responseString;
    
    //console.log(componentSettings);
    sendStatement();
}

function sendDragNDropFeedback(ele) {
    //console.log(ele);
    componentSettings.name = ele.divid;
    componentSettings.description = ele.question.trim();
    componentSettings.interactionType = "matching";
    var correctResultString = "";
    componentSettings.source = [];
    componentSettings.target = [];
    for (var i = 0; i < ele.dragPairArray.length; i++) {
        var pair = ele.dragPairArray[i];
        if (0 == i) {
            correctResultString = pair[0].textContent + "[.]"
                + pair[1].firstChild.textContent;
        } else {
            correctResultString += ("[,]" + pair[0].textContent + "[.]"
                + pair[1].firstChild.textContent);
        }
        var sourceOption = {};
        sourceOption["id"] = i.toString();
        sourceOption["description"] = {};
        sourceOption["description"][eBookConfig.language] = pair[0].textContent;
        componentSettings.source.push(sourceOption);
        
        var targetClass = {};
        targetClass["id"] = i.toString();
        targetClass["description"] = {};
        targetClass["description"][eBookConfig.language] = pair[1].firstChild.textContent;
        componentSettings.target.push(targetClass);
    }
    componentSettings.correctResponsesPattern = [];
    componentSettings.correctResponsesPattern.push(correctResultString);
    
    componentSettings.result["success"] = ele.correct;
    var responseString = "";
    for (var i = 0; i < ele.pregnantIndexArray.length; i++) {
        var sourceContext = "";
        if (0 <= ele.pregnantIndexArray[i]) {
            sourceContext = ele.dragPairArray[ele.pregnantIndexArray[i]][0].textContent;
        }
        if (0 == i) {
            responseString = sourceContext + "[.]"
                + ele.dragPairArray[i][1].firstChild.textContent;
        } else {
            responseString += ("[,]" + sourceContext + "[.]"
                + ele.dragPairArray[i][1].firstChild.textContent);
        }
    }
    componentSettings.result["response"] = responseString;
    
    //console.log(componentSettings);
    sendStatement();
}