#!/usr/bin/env python
# -*- coding: utf-8 -*-

from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.parsers.rst import directives
from os import path
from sphinx.application import Sphinx
import log
import re, os
from media import *
from assess import *
from parsons import *
from dragndrop import *

'''Video'''
'''Audio'''
'''Assessment questions'''
'''Multiple choice single answer'''
'''Multiple choice multiple answer'''
'''Fill in the blank'''
'''Sequencing'''
'''Matching'''



def on_builder_inited(app):
    """ Assess js/css files """
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'assess\js'),
          app.confdir
          ))
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'assess\css'),
          app.confdir
          ))
    
    app.add_stylesheet('fitb.css')
    app.add_stylesheet('bootstrap.min.css')
    
    app.add_javascript('jquery.js')
    app.add_javascript('runestonebase.js')
    app.add_javascript('mchoice.js')
    #app.add_javascript('timedmc.js')
    app.add_javascript('fitb.js')
    #app.add_javascript('timedfitb.js')
    #app.add_javascript('timed.js')
    
    """ Parsons js/css files """
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'parsons\js'),
          app.confdir
          ))
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'parsons\css'),
          app.confdir
          ))
    
    app.add_stylesheet('parsons.css')
    app.add_stylesheet('prettify.css')

    # includes parsons specific javascript headers
    # parsons-noconflict reverts underscore and
    # jquery to their original versions
    app.add_javascript('jquery.min.js')
    app.add_javascript('jquery-ui.min.js')
    app.add_javascript('prettify.js')
    app.add_javascript('underscore-min.js')
    app.add_javascript('lis.js')
    app.add_javascript('parsons.js')
    app.add_javascript('parsons-noconflict.js')
    
    """ Drag N Drop """
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'dragndrop\js'),
          app.confdir
          ))
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'dragndrop\css'),
          app.confdir
          ))
    app.add_stylesheet('dragndrop.css')
    
    app.add_javascript('dragndrop.js')
    app.add_javascript('timeddnd.js')
    
    """ xAPI """
    app.config.html_static_path.append( os.path.relpath(
          os.path.join(os.path.dirname(__file__), 'feedback\js'),
          app.confdir
          ))
    app.add_javascript('xapiwrapper.min.js')
    app.add_javascript('scripts.js')
    

def setup(app):
    
    app.add_config_value('interepub_config', 'interepub', '')
    
    # Video directive
    app.add_directive('video', Video)
    app.add_node(video, html=(visit_video_node, depart_video_node))
    
    # Audio directive
    app.add_directive('audio', Audio)
    app.add_node(audio, html=(visit_audio_node, depart_audio_node))
    
    # Multiple choice directives
    app.add_directive('mchoice', MChoice)
    app.add_directive('mchoicemf', MChoiceMF)
    app.add_directive('mchoicema', MChoiceMA)
    app.add_directive('mchoicerandommf', MChoiceRandomMF)
    app.add_directive('addbutton', AddButton)
    app.add_directive('qnum', QuestionNumber)
    app.add_directive('timed', TimedDirective)    
    app.add_node(TimedNode, html=(visit_timed_node, depart_timed_node))
    app.add_node(MChoiceNode, html=(visit_mc_node, depart_mc_node))

    # Fill in the blank directives
    app.add_directive('fillintheblank', FillInTheBlank)
    app.add_directive('blank', Blank)
    app.add_node(FITBNode, html=(visit_fitb_node, depart_fitb_node))
    app.add_node(BlankNode, html=(visit_blank_node, depart_blank_node))
    
    # Parsons directive
    app.add_directive('parsonsprob', ParsonsProblem)
    
    # Drag N Drop
    app.add_directive('dragndrop',DragNDrop)
    app.add_node(DragNDropNode, html=(visit_dnd_node, depart_dnd_node))
    
    # Load js/css files
    app.connect('builder-inited', on_builder_inited)