from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.parsers.rst import directives
import re
# Methods
def get_size(dic, key):
    if key not in dic:
        return None
    m = re.match("(\d+)(|%|)$", dic[key])
    return int(m.group(1)), m.group(2)

''' Video '''
# Node Class
class video(nodes.General, nodes.Element):
    pass

def visit_video_node(self, node):
    id = node['id']
    uri = node['uri']
    controls = node['controls']
    text = node['text']
    poster = node['poster']
    width = node['width']
    height = node['height']
    
    attrs = {
        'src': uri,
        }
    if id:
        attrs['ids'] = [id]
    
    if controls:
        attrs['controls'] = 'controls'
    else:
        attrs['autoplay'] = 'true'
    
    if poster:
        attrs['poster'] = poster
    if width:
        attrs['width'] = '%d' % width[0] if width[1] is None else '%d%s' % width
    if height:
        attrs['height'] = '%d' % height[0]
    
    attrs['onplay'] = "videoPlay(this)"
    
    self.body.append(self.starttag(node, 'video', **attrs))
    self.body.append('' if text is None else text)
    
def depart_video_node(self, node):
    self.body.append('</video>')

# Directives
class Video(Directive):
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        'id': directives.unchanged,
        'controls': directives.flag,
        'text': directives.unchanged,
        'poster': directives.uri,
        'width': directives.length_or_percentage_or_unitless,
        'height': directives.length_or_unitless,
    }
    
    '''
    .. video:: http://techslides.com/demos/sample-videos/small.mp4
       :id: sample_text
       :controls:
       :text: This is a sample video
       :poster: http://farm5.staticflickr.com/4115/4821890462_a0bce6010c.jpg
       :width:
       :height:
    '''
    
    def run(self):
        id = self.options['id'] if 'id' in self.options else None
        text = self.options['text'] if 'text' in self.options else None
        poster = self.options['poster'] if 'poster' in self.options else None
        
        width = get_size(self.options, 'width')
        height = get_size(self.options, 'height')
        
        return [video(uri = self.arguments[0], id = id, 
                      controls = 'controls' in self.options, text = text, 
                      poster = poster, width = width, height=height)]

''' Audio '''
# Node Class
class audio(nodes.General, nodes.Element):
    pass

def visit_audio_node(self, node):
    id = node['id']
    uri = node['uri']
    text = node['text']
    
    attrs = {
        'src': uri,
        'controls': 'controls'
        }
    if id:
        attrs['ids'] = [id]
    
    attrs['onplay'] = "audioPlay(this)"
    
    self.body.append(self.starttag(node, 'audio', **attrs))
    self.body.append('' if text is None else text)
    
def depart_audio_node(self, node):
    self.body.append('</audio>')

# Directives
class Audio(Directive):
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        'id': directives.unchanged,
        'text': directives.unchanged,
    }
    
    '''
    .. audio:: http://www.stephaniequinn.com/Music/Pachelbel%20-%20Canon%20in%20D%20Major.mp3
       :id: sample_text
       :text: This is a sample audio
    '''
    
    def run(self):
        id = self.options['id'] if 'id' in self.options else None
        text = self.options['text'] if 'text' in self.options else None
        
        return [audio(uri = self.arguments[0], id = id, text = text)]
