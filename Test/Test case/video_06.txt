.. FirstDoc documentation master file, created by
   sphinx-quickstart on Fri May 15 15:18:05 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FirstDoc's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

Test case: video_06

start of test case

.. video:: http://techslides.com/demos/sample-videos/small.webm
   :id: sample_text
   :controls:
   :text: WebM video

end of test case


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

